#
#  Be sure to run `pod spec lint OpenKeyChainLib.podspec' to ensure this is a
#  valid spec and to remove all comments including this before submitting the spec.
#
#  To learn more about Podspec attributes see http://docs.cocoapods.org/specification.html
#  To see working Podspecs in the CocoaPods repo see https://github.com/CocoaPods/Specs/
#

Pod::Spec.new do |s|
  s.name         = "OpenKeyChainLib"
  s.version      = "0.0.2"
  s.summary      = "OpenKeyChain reference implementation for iOS."
  s.description  = <<-DESC
                   A reference implementation for OpenKeyChain blockchain based auth scheme.
                   DESC

  s.homepage     = "https://www.blocko.io"
  s.license      = "MIT"
  s.author             = { "Won-Beom Kim" => "shepelt@blocko.io" }
  s.platform     = :ios, "6.0"
  s.source       = { :git => "https://bitbucket.org/cloudwallet/openkeychain-objc", :tag => "0.0.2" }
  s.source_files  = 'OpenKeyChainLib', 'OpenKeyChainLib/**/*.{h,m}'
  s.public_header_files = 'OpenKeyChainLib/**/*.h'
  s.requires_arc = true
end
