//
//  OpenKeyChainLib.m
//  OpenKeyChainLib
//
//  Created by Won Beom Kim on 2016. 2. 16..
//  Copyright © 2016년 Blocko. All rights reserved.
//

#import "OpenKeyChainLib.h"
#import <CoreBitcoin/CoreBitcoin.h>

@implementation OpenKeyChainLib {
    NSTimeInterval threshold;
}

- (id) init {
    self->threshold = 30.0 * 60; // 30 minutes
    return self;
}

- (id) initWithTimeoutThreshold:(NSTimeInterval)newThreshold {
    if (self = [super init]) {
        self->threshold = newThreshold;
    }
    return self;
}

- (NSString*) createPrivateKey
{
    BTCKey* key = [[BTCKey alloc] init];
    return [key WIF];
}

- (NSString*) deriveAddress:(NSString*)privateKey
{
    BTCKey* key = [[BTCKey alloc] initWithWIF:privateKey];
    return [[[key privateKeyAddress] publicAddress] string];
}

- (NSString*) formatChallengeJsonWithoutSignature:(NSString*)context timestamp:(NSString*)timestamp
{
    return [NSString stringWithFormat:@"{\"context\":\"%@\",\"timestamp\":\"%@\"}", context, timestamp];
}

- (NSDate *)parseRFC3339:(NSString *)dateString {
    
    // Create date formatter
    static NSDateFormatter *dateFormatter = nil;
    if (!dateFormatter) {
        NSLocale *en_US_POSIX = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
        dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setLocale:en_US_POSIX];
        [dateFormatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
    }
    
    // Process date
    NSDate *date = nil;
    NSString *RFC3339String = [[NSString stringWithString:dateString] uppercaseString];
    RFC3339String = [RFC3339String stringByReplacingOccurrencesOfString:@"Z" withString:@"-0000"];
    // Remove colon in timezone as iOS 4+ NSDateFormatter breaks. See https://devforums.apple.com/thread/45837
    if (RFC3339String.length > 20) {
        RFC3339String = [RFC3339String stringByReplacingOccurrencesOfString:@":"
                                                                 withString:@""
                                                                    options:0
                                                                      range:NSMakeRange(20, RFC3339String.length-20)];
    }
    if (!date) { // 1996-12-19T16:39:57-0800
        [dateFormatter setDateFormat:@"yyyy'-'MM'-'dd'T'HH':'mm':'ssZZZ"];
        date = [dateFormatter dateFromString:RFC3339String];
    }
    if (!date) { // 1937-01-01T12:00:27.87+0020
        [dateFormatter setDateFormat:@"yyyy'-'MM'-'dd'T'HH':'mm':'ss.SSSZZZ"];
        date = [dateFormatter dateFromString:RFC3339String];
    }
    if (!date) { // 1937-01-01T12:00:27
        [dateFormatter setDateFormat:@"yyyy'-'MM'-'dd'T'HH':'mm':'ss"];
        date = [dateFormatter dateFromString:RFC3339String];
    }
    if (!date) NSLog(@"Could not parse RFC3339 date: \"%@\" Possibly invalid format.", dateString);
    return date;
    
}

- (BOOL) checkChallenge:(NSString*)challenge authorityAddress:(NSString*)authorityAddress
{
    NSError* error;
    NSDictionary* json = [NSJSONSerialization JSONObjectWithData:[challenge dataUsingEncoding:NSUTF8StringEncoding] options:kNilOptions error:&error];
    
    if (error) {
        return false;
    }
    
    // check timestamp
    NSDate *date = [self parseRFC3339: json[@"timestamp"]];
    NSTimeInterval elapsed = 0 - date.timeIntervalSinceNow;
    if (elapsed > self->threshold) {
        return false;
    }
    
    NSString* jsonString = [self formatChallengeJsonWithoutSignature: json[@"context"] timestamp: json[@"timestamp"]];
    
    NSData *signature = [[NSData alloc] initWithBase64EncodedString:json[@"signature"] options:0];
    BTCKey* key = [BTCKey verifySignature:signature forMessage:jsonString];
    NSString* address = key.compressedPublicKeyAddress.string;
    
    if ([key isValidSignature:signature forMessage:jsonString]) {
    } else {
        return false;
    }
    
    if ([address isEqualToString:authorityAddress]) {
    } else {
        return false;
    }
    
    return true;
}


- (NSString*) formatResponseJsonWithoutSignature:(NSString*)certificate challenge:(NSString*)challenge timestamp:(NSString*)timestamp
{
    return [NSString stringWithFormat:@"{\"certificate\":\"%@\",\"challenge\":\"%@\",\"timestamp\":\"%@\"}", certificate, challenge, timestamp];
}

- (NSString*) formatResponseJsonWithSignature:(NSString*)signature certificate:(NSString*)certificate challenge:(NSString*)challenge timestamp:(NSString*)timestamp
{
    return [NSString stringWithFormat:@"{\"signature\":\"%@\",\"certificate\":\"%@\",\"challenge\":\"%@\",\"timestamp\":\"%@\"}", signature, certificate, challenge, timestamp];
}

- (NSString*) getRFC3339Timestamp {
    NSDate *now = [[NSDate alloc] init];
    NSTimeZone *localTimeZone = [NSTimeZone systemTimeZone];
    NSDateFormatter *rfc3339DateFormatter = [[NSDateFormatter alloc] init];
    NSLocale *enUSPOSIXLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    
    [rfc3339DateFormatter setLocale:enUSPOSIXLocale];
    [rfc3339DateFormatter setDateFormat:@"yyyy'-'MM'-'dd'T'HH':'mm':'ssZ"];
    [rfc3339DateFormatter setTimeZone:localTimeZone];
    
    NSString *dateString = [rfc3339DateFormatter stringFromDate:now];
    return dateString;
}

- (NSString*) createResponse:(NSString*)challenge privateKey:(NSString*)privateKey
{
    NSData *dataPayload = [challenge dataUsingEncoding:NSUTF8StringEncoding];
    NSString *base64Encoded = [dataPayload base64EncodedStringWithOptions:0];
    
    NSString *timestamp = [self getRFC3339Timestamp];
    NSString *address = [self deriveAddress:privateKey];
    if (address == nil) {
        return nil;
    };
    NSString *before = [self formatResponseJsonWithoutSignature: address challenge: base64Encoded timestamp: timestamp];
    BTCKey *key = [[BTCKey alloc] initWithWIF:privateKey];
    if (key == nil) {
        return nil;
    }
    NSData *signatureData = [key signatureForMessage:before];

    NSString* signature = [signatureData base64EncodedStringWithOptions:0];
    NSString *after =[self formatResponseJsonWithSignature:signature certificate: address challenge: base64Encoded timestamp: timestamp];
    
    return after;
}

@end
