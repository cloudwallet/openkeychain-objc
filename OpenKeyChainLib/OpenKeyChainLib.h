//
//  OpenKeyChainLib.h
//  OpenKeyChainLib
//
//  Created by Won Beom Kim on 2016. 2. 16..
//  Copyright © 2016년 Blocko. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OpenKeyChainLib : NSObject
-(id) initWithTimeoutThreshold:(NSTimeInterval)newThreshold;
-(NSString*)createPrivateKey;
-(NSString*) deriveAddress:(NSString*)privateKey;
-(BOOL) checkChallenge:(NSString*)challenge authorityAddress:(NSString*)authorityAddress;
-(NSString*) createResponse:(NSString*)challenge privateKey:(NSString*)privateKey;
@end